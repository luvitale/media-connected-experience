<?php
/**
 * Plugin Name: Media Connected Experience
 * Plugin URI: https://www.unesma.net
 * Description: Wordpress plugin to connect Instagram media.
 * Version: 1.1
 * Author: Luciano Nahuel Vitale
 * Author URI: https://luvitale.net
 */
require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/vendor/scssphp/scssphp/scss.inc.php';
use ScssPhp\ScssPhp\Compiler;
use EspressoDev\InstagramBasicDisplay\InstagramBasicDisplay;

$root_dir = dirname(__DIR__) . '/media-connected-experience';

$dotenv = Dotenv\Dotenv::createUnsafeImmutable($root_dir);
if (file_exists($root_dir . '/.env')) {
    $dotenv->load();
    $dotenv->required(['IG_APP_ID', 'IG_APP_SECRET', 'IG_REDIRECT_URI']);
}

add_action( 'wp_enqueue_scripts', 'wpmce_enqueue_scripts' );
function wpmce_enqueue_scripts() {
    wp_register_script( 'mce-swiper-js', plugins_url( "node_modules/swiper/dist/js/swiper.min.js", __FILE__ ) );
    wp_register_style( 'mce-swiper-css',  plugins_url( "node_modules/swiper/dist/css/swiper.min.css", __FILE__ ) );
    wp_register_script( 'mce-is-js', plugins_url( "js/instagram-swiper.js", __FILE__ ) );
    wp_register_style( 'wp-instagram-swiper', plugins_url( "css/instagram-swiper.css", __FILE__ ) );
}

class IGToken {
    private static function set_token($token) {
        global $wpdb;
        $ig_token_table = $wpdb->prefix . "ig_tokens";

        $data = array('token' => $token);
        $where = array('id' => 1);
        $wpdb->update($ig_token_table, $data, $where);
    }
}

function wpdocs_register_mce_page() {
    add_menu_page(
        __( 'MCE', 'mce-wp' ),
        __( 'MCE', 'mce-wp' ), 
        'administrator', 
        'mce',
        'mce_dashboard'
    );

    add_submenu_page( 'mce',
        __( 'Auth', 'mce-wp' ),
        __( 'Auth', 'mce-wp' ), 
        'administrator',
        'mce_auth',
        'mce_auth' 
    );
}
add_action( 'admin_menu', 'wpdocs_register_mce_page' );

function process_token_code() {
    global $pagenow;
    if ( $pagenow == 'index.php' ) {
        if ( isset($_GET['code']) ) {
            $code = $_GET['code'];
            
            $instagram = ig_basic_display();

            // Get the short lived access token (valid for 1 hour)
            $token = $instagram->getOAuthToken($code, true);
            
            // Exchange this token for a long lived token (valid for 60 days)
            $token = $instagram->getLongLivedToken($token, true);

            IGToken::set_token($token);
            
            ?>
            <?php ob_start(); ?>
            <div class="notice notice-info is-dismissible">
                <p>Your token is: <?= $token ?></p>
            </div>
            <?php ob_get_contents();
        }
    }
}
register_activation_hook( __FILE__,  'process_token_code');

function ig_basic_display() {
    return new InstagramBasicDisplay([
        'appId' => getenv('IG_APP_ID'),
        'appSecret' => getenv('IG_APP_SECRET'),
        'redirectUri' => getenv('IG_REDIRECT_URI')
    ]);
}

function mce_dashboard() {
    echo "Media Connected Experience!";
}

function mce_auth() {
    $instagram = ig_basic_display();
    ob_start();
    ?>
    <a href='<?= $instagram->getLoginUrl() ?>'>Login with Instagram</a>
    <?php ob_get_contents();
}

function print_inline_script($stylesheet) {
    ?>
    <style type="text/css">
        <?php
        $scss = new Compiler();
    
        echo $scss->compile('@import "'. __DIR__ . '/' . $stylesheet . '";');
        ?>
    </style>
    <?php
}

function shortcode_instagram_swiper() {
    wp_enqueue_script( 'mce-swiper-js' );
    wp_enqueue_style( 'mce-swiper-css' );
    wp_enqueue_script( 'mce-is-js' );
    wp_enqueue_style( 'wp-instagram-swiper' );


    $instagram = ig_basic_display();

    global $wpdb;
    $ig_token_table = $wpdb->prefix . 'ig_tokens';
    $results = $wpdb->get_results("SELECT * FROM $ig_token_table");
    $token = $results[0]->token;
    ?>
    <?php if ($token): ?>
        <?php
        // Set user access token
        $instagram->setAccessToken($token);
        $media = $instagram->getUserMedia();
        ?>

        <?php ob_start(); ?>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($media->data as $entry): ?>
                <?php if ($entry->media_type != 'VIDEO'): ?>
                <a href="<?= $entry->permalink ?>" target="_blank" class="swiper-slide" style="background-image:url(<?= $entry->media_url ?>)"></a>
                <?php endif ?>
                <?php endforeach ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper-pagination-white"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
        </div>
    <?php else: ?>
        Token no cargado
    <?php endif ?>
    <?php ob_get_contents();
}
add_shortcode('instagram-swiper', 'shortcode_instagram_swiper');